/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DecisionTreeVariants;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Siamul Karim Khan
 */
public class AdaBoost {
    String[][] dataset;
    ArrayList<Integer> attrList;
    DecisionTree[] treeList;
    double[] B;
    int k;
    AdaBoost(String[][] trainSet,  ArrayList<Integer> attrList, int k)
    {
        dataset = trainSet;
        this.attrList = attrList;
        this.k = k;
        treeList = new DecisionTree[k];
        B = new double[k];
    }
    
    void learn()
    {
        double[] weights = new double[dataset.length - 1];
        Arrays.fill(weights, 1.0/(double)weights.length);
        for(int r = 0; r<k; r++)
        {
            double sum = 0;
            for(int i = 0; i<weights.length; i++)
            {
                sum += weights[i];
            }
            double[] probabilities = new double[weights.length];
            for(int i = 0; i<weights.length; i++)
            {
                probabilities[i] = weights[i]/sum;
            }
            //DataSetOperations.printArray(probabilities);
            String[][] dataStump = DataSetOperations.randomlySelectSet(dataset, probabilities, 3*(dataset.length-1)/4);
            treeList[r] = new DecisionTree();
            treeList[r].learn(dataStump, (ArrayList<Integer>) attrList.clone(), 1);
            String[][] results = treeList[r].getLabels(dataset);
            double er = 0;
            //int missClassNo = 0;
            for(int i = 1; i<results.length; i++)
            {
                if(results[i][dataset[0].length - 1].compareTo(dataset[i][dataset[0].length - 1]) != 0)
                {
                    er += probabilities[i-1];
                    //System.out.println(er);
              //      missClassNo++;
                }
            }
            //System.out.println(missClassNo);
            if(er > 0.5)
            {
                k = r-1;
                System.out.println("er > 0.5, exiting...");
                break;
            }
            B[r] = er/(1-er);
            for(int i = 1; i<results.length; i++)
            {
                if(results[i][dataset[0].length - 1].compareTo(dataset[i][dataset[0].length - 1]) == 0)
                {
                    weights[i-1] *= B[r];
                }
            }
        }
    }
    public double[] test(String[][] table)
    {
        String[][][] results = new String[k][][];
        int[] AdaBoostResult = new int[table.length];
        for(int i = 0; i<k; i++)
        {
            results[i] = treeList[i].getLabels(table);
        }
        double[] score = new double[2];
        for(int i = 1; i<table.length; i++)
        {
            score[0] = 0;
            score[1] = 0;
            for(int j = 0; j<k; j++)
            {
                if(results[j][i][table[0].length - 1].compareTo("0") == 0)
                {
                    score[0] += Math.log10(1.0/B[j]);
                }
                else
                {
                    score[1] += Math.log10(1.0/B[j]);
                }
            }
            if(score[0] > score[1])
                AdaBoostResult[i] = 0;
            else 
                AdaBoostResult[i] = 1;
        }
        double truePositive = 0; 
        double trueNegative = 0;
        double falsePositive = 0;
        double falseNegative = 0;
        double correct = 0;
        for(int i = 1; i<table.length; i++)
        {
            if(AdaBoostResult[i] == 0)
            {
                if(table[i][table[0].length - 1].compareTo("0") == 0)
                {
                    trueNegative++;
                }
                else
                {
                    falseNegative++;
                }
            }
            else if(AdaBoostResult[i] == 1)
            {
                if(table[i][table[0].length - 1].compareTo("0") == 0)
                {
                    falsePositive++;
                }
                else
                {
                    truePositive++;
                }
            }
        }
        double[] retValue = new double[4];
        retValue[0] = (truePositive+trueNegative)/(truePositive+trueNegative+falsePositive+falseNegative);
        retValue[1] = truePositive/(truePositive+falsePositive);
        retValue[2] = truePositive/(truePositive+falseNegative);
        retValue[3] = 2*(retValue[1]*retValue[2])/(retValue[1]+retValue[2]);
        return retValue;
    }
}
