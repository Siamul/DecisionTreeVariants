/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DecisionTreeVariants;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Siamul Karim Khan
 */
public class SemiSupervisedLearner {
    DecisionTree semiT;
    DecisionTree fullT;
    DecisionTree onlyLT;
    String[][] DL;
    String[][][] DU;
    static int DLsize = 50;
    int tenPercentSize;
    int noOfUnlabeledSets;
    ArrayList<Integer> attrList;
    public SemiSupervisedLearner(String[][] trainSet, ArrayList<Integer> attrList)
    {
        fullT = new DecisionTree();
        fullT.learn(trainSet, attrList, -1);
        String[][][] sepSet = DataSetOperations.separateSet(trainSet, DLsize);
        DL = sepSet[0];
        String[][] unlabeledData = sepSet[1];
        tenPercentSize = (int) (0.1*trainSet.length);
        noOfUnlabeledSets = unlabeledData.length/tenPercentSize;
        DU = new String[noOfUnlabeledSets][][];
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i=1; i<unlabeledData.length; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        for(int i = 0; i<noOfUnlabeledSets; i++)
        {
            DU[i] = new String[tenPercentSize + 1][];
            DU[i][0] = unlabeledData[0].clone();
            for(int j = 0; j<tenPercentSize; j++)
            {
                DU[i][j + 1] = unlabeledData[list.get(i*tenPercentSize + j)].clone();
            }
        }         
        this.attrList = attrList;
    }
    public void learn()
    {
        semiT = new DecisionTree();
        onlyLT = new DecisionTree();
        semiT.learn(DL, attrList, -1);
        onlyLT.learn(DL, attrList, -1);
        String[][] TDataSet = DL.clone();
        for(int i = 0; i<noOfUnlabeledSets; i++)
        {
            //labelling 10% of the unlabeled data
            String[][] labeledDU = semiT.getLabels(DU[i]);
            //creating the data set for the new tree
            TDataSet = DataSetOperations.concatTable(TDataSet, labeledDU);
            //learning the new tree
            semiT.learn(TDataSet, attrList, -1);
        }
        
    }
    

    double[] onlyLTest(String[][] table)
    {
       return onlyLT.test(table);
    }
    
    double[] fullTest(String[][] table)
    {
        return fullT.test(table);
    }
    
    double[] semiTest(String[][] table)
    {
        return semiT.test(table);
    }
    

}
