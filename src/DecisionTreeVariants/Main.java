/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DecisionTreeVariants;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Siamul Karim Khan
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    static int iter = 1000;
    public static void main(String[] args) {
        // TODO code application logic here
        //String csvFile = "mitchell_data_set.csv";
        String csvFile = "assignment1_data_set.csv";
        String line;
        String[] tableHeader;
        String[] newHeader;
        ArrayList<String[]> temp = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String tHeader = br.readLine();
            tableHeader = tHeader.split(",");
            newHeader = new String[tableHeader.length];
            for(int i = 0; i<tableHeader.length; i++)
            {
                newHeader[i] = String.valueOf(i);
                //newHeader[i] = "a";
            }
            temp.add(newHeader);
            while ((line = br.readLine()) != null) {
                String[] row = line.split(",");
                temp.add(row);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        String[][] table = new String[temp.size()][];
        table = temp.toArray(table);
        //semiSupervisedAndValidation(table);
        ensembleLearning(table);
        
    }
    static void ensembleLearning(String[][] table)
    {
        int[] k = {5,10,20,30};
        ArrayList<Integer> attrList = new ArrayList<>();
        for(int i = 0; i<table[0].length - 1; i++)
        {
            attrList.add(i);
        }
        for(int r = 0 ; r<k.length; r++)
        {
            double adaAccuracy = 0, adaPrecision = 0, adaRecall = 0, adaF1 = 0;
            for(int i = 0; i<iter; i++)
            {
                String[][][] setSel = DataSetOperations.separateSet(table, 80);
                String[][] trainSet = setSel[0];
                String[][] testSet = setSel[1];
                AdaBoost boostedLearner = new AdaBoost(trainSet, (ArrayList<Integer>)attrList.clone(), k[r]);
                boostedLearner.learn();
                double[] results = boostedLearner.test(testSet);
                adaAccuracy += results[0];
                adaPrecision += results[1];
                adaRecall += results[2];
                adaF1 += results[3];
            }
            adaAccuracy /= iter;
            adaPrecision /= iter;
            adaRecall /= iter;
            adaF1 /= iter;
            System.out.println("=================================================================================================================================");
            System.out.println("Statistics for AdaBoost with k = " + k[r]);
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------");
            System.out.println("Average accuracy over " + iter + " runs = " + adaAccuracy);
            System.out.println("Average precision over " + iter + " runs = " + adaPrecision);
            System.out.println("Average recall over " + iter + " runs = " + adaRecall);
            System.out.println("Average F1-score over " + iter + " runs = " + adaF1);
        }
        double stumpAccuracy = 0, stumpPrecision = 0, stumpRecall = 0, stumpF1 = 0;
        double id3Accuracy = 0, id3Precision = 0, id3Recall = 0, id3F1 = 0;
        for(int i = 0; i<iter; i++)
        {
            String[][][] setSel = DataSetOperations.separateSet(table, 80);
            String[][] trainSet = setSel[0];
            String[][] testSet = setSel[1];
            DecisionTree stumpLearner = new DecisionTree();
            stumpLearner.learn(trainSet, (ArrayList<Integer>)attrList.clone(), 1);
            //stumpLearner.print();
            double[] results = stumpLearner.test(testSet);
            stumpAccuracy += results[0];
            stumpPrecision += results[1];
            stumpRecall += results[2];
            stumpF1 += results[3];
            DecisionTree id3Learner = new DecisionTree();
            id3Learner.learn(trainSet, (ArrayList<Integer>)attrList.clone(), -1);
            results = id3Learner.test(testSet);
            id3Accuracy += results[0];
            id3Precision += results[1];
            id3Recall += results[2];
            id3F1 += results[3];
        }
        stumpAccuracy /= iter;
        stumpPrecision /= iter;
        stumpRecall /= iter;
        stumpF1 /= iter;
        id3Accuracy /= iter;
        id3Precision /= iter;
        id3Recall /= iter;
        id3F1 /= iter;
        System.out.println("=================================================================================================================================");
        System.out.println("Statistics for decision stump:" );
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Average accuracy over " + iter + " runs = " + stumpAccuracy);
        System.out.println("Average precision over " + iter + " runs = " + stumpPrecision);
        System.out.println("Average recall over " + iter + " runs = " + stumpRecall);
        System.out.println("Average F1-score over " + iter + " runs = " + stumpF1);
        System.out.println("=================================================================================================================================");
        System.out.println("Statistics for decision tree with no constraints:");
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Average accuracy over " + iter + " runs = " + id3Accuracy);
        System.out.println("Average precision over " + iter + " runs = " + id3Precision);
        System.out.println("Average recall over " + iter + " runs = " + id3Recall);
        System.out.println("Average F1-score over " + iter + " runs = " + id3F1);
        System.out.println("=================================================================================================================================");
    }
    static void semiSupervisedAndValidation(String[][] table)
    {
        double semiTAccuracy = 0, semiTPrecision = 0, semiTRecall = 0, semiTF1 = 0;
        double onlyLTAccuracy = 0, onlyLTPrecision = 0, onlyLTRecall = 0, onlyLTF1 = 0;
        double fullTAccuracy = 0, fullTPrecision = 0, fullTRecall = 0, fullTF1 = 0;
        ArrayList<Integer> attrList = new ArrayList<>();
        for(int i = 0; i<table[0].length - 1; i++)
        {
            attrList.add(i);
        }
        for(int j = 0; j<iter; j++)
        {
            String[][][] setSel = DataSetOperations.separateSet(table.clone(), 80);
            String[][] trainSet = setSel[0];
            String[][] testSet = setSel[1];
            //printTable(trainSet);
           //System.out.println("============================");
            //printTable(testSet);

            SemiSupervisedLearner learner = new SemiSupervisedLearner(trainSet, (ArrayList<Integer>)attrList.clone());
            learner.learn();
            
            double[] result = learner.semiTest(testSet);
            semiTAccuracy += result[0];
            semiTPrecision += result[1];
            semiTRecall += result[2];
            semiTF1 += result[3];
            result = learner.onlyLTest(testSet);
            onlyLTAccuracy += result[0];
            onlyLTPrecision += result[1];
            onlyLTRecall += result[2];
            onlyLTF1 += result[3];
            result = learner.fullTest(testSet);
            fullTAccuracy += result[0];
            fullTPrecision += result[1];
            fullTRecall += result[2];
            fullTF1 += result[3];
        }
        semiTAccuracy /= iter;
        fullTAccuracy /= iter;
        onlyLTAccuracy /= iter;
        semiTPrecision /= iter;
        fullTPrecision /= iter;
        onlyLTPrecision /= iter;
        semiTRecall /= iter;
        fullTRecall /= iter;
        onlyLTRecall /= iter;
        semiTF1 /= iter;
        fullTF1 /= iter;
        onlyLTF1 /= iter;
        System.out.println("=================================================================================================================================");
        System.out.println("Statistics for semi-supervised learned tree");
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Average accuracy over " + iter + " runs = " + semiTAccuracy);
        System.out.println("Average precision over " + iter + " runs = " + semiTPrecision);
        System.out.println("Average recall over " + iter + " runs = " + semiTRecall);
        System.out.println("Average F1-score over " + iter + " runs = " + semiTF1);
        System.out.println("=================================================================================================================================");
        System.out.println("Statistics for fully-supervised learned tree with all data as labeled");
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Average accuracy over " + iter + " runs = " + fullTAccuracy);
        System.out.println("Average precision over " + iter + " runs = " + fullTPrecision);
        System.out.println("Average recall over " + iter + " runs = " + fullTRecall);
        System.out.println("Average F1-score over " + iter + " runs = " + fullTF1);
        System.out.println("=================================================================================================================================");
        System.out.println("Statistics for fully-supervised learned tree with " + SemiSupervisedLearner.DLsize+ "% unlabeled data");
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Average accuracy over " + iter + " runs = " + onlyLTAccuracy);
        System.out.println("Average precision over " + iter + " runs = " + onlyLTPrecision);
        System.out.println("Average recall over " + iter + " runs = " + onlyLTRecall);
        System.out.println("Average F1-score over " + iter + " runs = " + onlyLTF1);
        System.out.println("=================================================================================================================================");
        System.out.println("Accuracies for k-fold validations: ");
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------");
        for(int k = 5; k<=20; k*=2)
        {
            System.out.println("Accuracy for k = " + k + ": " + Validation.kFoldValidation(table.clone(), (ArrayList<Integer>)attrList.clone(), k));
        }
        System.out.println("=================================================================================================================================");
        System.out.println("Accuracy for leave-one-out validation: " + Validation.leaveOneOutValidation(table.clone(), (ArrayList<Integer>)attrList.clone()));
        System.out.println("=================================================================================================================================");
    }

}
