/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DecisionTreeVariants;

/**
 *
 * @author Siamul Karim Khan
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 *
 * @author Siamul Karim Khan
 */
public class DataSetOperations {
    public static String[][] randomlySelectSet(String[][] table, double[] probabilities, int size)
    {
        double[] cumulativeProb = new double[probabilities.length];
        cumulativeProb[0] = probabilities[0];
        for(int i = 1; i<probabilities.length; i++)
        {
           cumulativeProb[i] = cumulativeProb[i-1] + probabilities[i];
        }
        //System.out.println(Math.round(cumulativeProb[cumulativeProb.length - 1]));
        String[][] retV = new String[size+1][];
        retV[0] = Arrays.copyOf(table[0], table[0].length);
        Random gen = new Random();
        for(int i = 1; i<size+1; i++)
        {
            double s = gen.nextDouble();
            int index;
            for(index = 0; index < cumulativeProb.length; index++)
            {
                if(s < cumulativeProb[index])
                {
                    break;
                }
            }
            retV[i] = Arrays.copyOf(table[index+1], table[index+1].length);
        }
        return retV;
    }
    public static String[][][] separateSet(String[][] table, int percent)
    {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i=1; i<table.length; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        int trainSetIndex = ((table.length - 1)*percent)/100;
        ArrayList<String[]> trainSet = new ArrayList<>();
        ArrayList<String[]> testSet = new ArrayList<>();
        trainSet.add(table[0]);
        testSet.add(table[0]);
        for(int i = 0; i<trainSetIndex; i++)
        {
            trainSet.add(table[list.get(i)]);
        }
        for(int i = trainSetIndex; i<list.size(); i++)
        {
            testSet.add(table[list.get(i)]);
        }
        String[][][] retVal = new String[2][][];
        retVal[0] = new String[trainSet.size()][];
        retVal[1] = new String[testSet.size()][];
        retVal[0] = trainSet.toArray(retVal[0]);
        retVal[1] = testSet.toArray(retVal[1]);
        return retVal;
    }
    public static String[][][] makeKSets(String[][] table, int k)
    {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i=1; i<table.length; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        int trainSetIndex = (table.length - 1)/k;
        ArrayList<String[]> set = new ArrayList<>();
        String[][][] retVal = new String[k][][];
        for(int i = 0; i<k; i++)
        {
            set.clear();
            set.add(table[0]);
            for(int j = 0; j<trainSetIndex; j++)
            {
                set.add(table[list.get(trainSetIndex*i + j)]);
            }
            retVal[i] = new String[trainSetIndex][];
            retVal[i] = set.toArray(retVal[i]);
        }
        return retVal;
    }
    //concatenating table keeping only one header
    public static String[][] concatTable(String[][] table1, String[][] table2)
    {
        String[][] retV = new String[table1.length + table2.length - 1][];
        for(int i = 0; i<table1.length; i++)
        {
            retV[i] = table1[i];
        }
        for(int i = table1.length; i<retV.length; i++)
        {
            retV[i] = table2[i+1-table1.length];
        }
        return retV;
    }
    
    public static void printTable(String[][] table)
    {
        for(int i = 0; i<table.length; i++)
        {
            for(int j = 0; j<table[0].length; j++)
            {
                System.out.print(table[i][j]);
                if(j<table[0].length - 1) System.out.print(",");
            }
            System.out.println();
        }
    }
    public static <T> void printArray(T[] array)
    {
        for(int i = 0; i<array.length-1; i++)
        {
            System.out.print(array[i] + ", ");
        }
        System.out.println(array[array.length - 1]);
    }
}

