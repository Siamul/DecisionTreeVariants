/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DecisionTreeVariants;

import java.util.ArrayList;
import java.util.Arrays;
/**
 *
 * @author Siamul Karim Khan
 */
public class Validation {
    public static double kFoldValidation(String[][] table, ArrayList<Integer> attrList, int k)
    {
        String[][][] sets = DataSetOperations.makeKSets(table.clone(), k);
        double totalAccuracy = 0;
        String[][] trainData;
        String[][] testData;
        DecisionTree dt = new DecisionTree();
        for(int i = 0; i<k; i++)
        {
            if(i == 0) trainData = sets[1];
            else trainData = sets[0];
            testData = sets[i];
            for(int j = 1; j<k; j++)
            {
                if(j != i)
                {
                    trainData = DataSetOperations.concatTable(trainData, sets[j]);
                }
            }
            dt.learn(trainData, (ArrayList<Integer>)attrList.clone(), -1);
            double[] result = dt.test(testData);
            totalAccuracy += result[0];
        }
        return (totalAccuracy/(double)k);
    }
    
    public static double leaveOneOutValidation(String[][] table, ArrayList<Integer> attrList)
    {
        double totalAccuracy = 0;
        String[][] trainData = new String[table.length - 1][];
        String[][] testData = new String[2][];
        trainData[0] = table[0];
        testData[0] = table[0];
        DecisionTree dt = new DecisionTree();
        for(int i = 1; i<table.length; i++)
        {  
            testData[1] = table[i];
            int index = 1;
            for(int j = 1; j<table.length; j++)
            {
                if(j != i) trainData[index++] = table[j];
            }
            dt.learn(trainData.clone(), (ArrayList<Integer>)attrList.clone(), -1);
            double[] result = dt.test(testData);
            totalAccuracy += result[0];
        }
        return (totalAccuracy/(double)(table.length-1));
    }
}
